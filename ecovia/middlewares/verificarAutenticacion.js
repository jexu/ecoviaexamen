const connection = require('../config/db');

// Middleware para verificar el estado de autenticación del usuario
function verificarAutenticacion(req, res, next) {
  const usuarioId = req.session.usuarioId;

  if (usuarioId) {
    // Almacena el ID del usuario en una variable local
    res.locals.usuarioId = usuarioId;

    // Si hay un usuario conectado, consultar la base de datos para obtener la información del usuario
    connection.query('SELECT * FROM usuarios WHERE id = ?', [usuarioId], (error, results) => {
      if (error) {
        console.error('Error al consultar la base de datos:', error);
        res.locals.usuarioConectado = false; // El usuario no está autenticado
      } else {
        if (results.length > 0) {
          // Usuario autenticado, pasar la información del usuario a la vista
          res.locals.usuarioConectado = true;
          res.locals.nombreUsuario = results[0].nombre_usuario;
        } else {
          res.locals.usuarioConectado = false; // El usuario no está autenticado
        }
      }

      next();
    });
  } else {
    res.locals.usuarioConectado = false; // El usuario no está autenticado
    next();
  }
}

module.exports = { verificarAutenticacion };
