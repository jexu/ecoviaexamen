const express = require('express');
const path = require('path');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const session = require('express-session');
const csrf = require('csurf');
const cookieParser = require('cookie-parser');
const { verificarAutenticacion } = require('./middlewares/verificarAutenticacion');

// Opciones
app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// Middlewares
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({
  secret: 'secreto',
  resave: false,
  saveUninitialized: false
}));

// Configurar csurf
const csrfProtection = csrf({ cookie: true });

// Generar y agregar el token CSRF a todas las vistas renderizadas
app.use(function(req, res, next) {
  csrfProtection(req, res, function(err) {
    if (err) {
      return next(err);
    }
    res.locals.csrfToken = req.csrfToken();
    next();
  });
});

app.use(verificarAutenticacion);

// Rutas
const rutasAplicacion = require('./routes/principal');
const seguridad = require('./routes/seguridad/utils');
app.use('/', csrfProtection, rutasAplicacion);

// Diseño
app.use(express.static(path.join(__dirname, 'public')));

app.listen(app.get('port'), () => {
  console.log('Escuchando en el puerto', app.get('port'));
});
