const connection = require('../config/db')

function mostrarPaginaInicio(req, res) {
  // Obtener usuarioConectado y nombreUsuario de res.locals
  const { usuarioConectado, nombreUsuario } = res.locals;

  // Renderizar la vista y pasar las variables a la vista
  res.render('inicio', { usuarioConectado, nombreUsuario });
}
  module.exports = {
    mostrarPaginaInicio
  };