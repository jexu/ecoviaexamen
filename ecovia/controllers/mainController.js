const connection = require('../config/db');
const seguridad = require('../routes/seguridad/utils');
const jwt = require('jsonwebtoken');
const config = require('../config/config');

const { limpiarRequest } = require('../routes/seguridad/utils');

// Controlador para mostrar el formulario de registro
function registroform(req, res) {
  res.render('registro');
}

// Controlador para procesar el registro
function procesarRegistro(req, res) {
  const { nombre, correo, contraseña } = req.body;
  const contraseñaCifrada = seguridad.btoa(contraseña);

  // Insertar un nuevo usuario en la base de datos
  connection.query('INSERT INTO usuarios (nombre_usuario, correo, password) VALUES (?, ?, ?)', [nombre, correo, contraseñaCifrada], (error, results) => {
    if (error) {
      console.error('Error al insertar en la base de datos:', error);
      res.render('registro', { error: 'Error al procesar el registro' });
    } else {
      // Registro exitoso
      res.redirect('/iniciar_sesion'); // Redirigir al formulario de inicio de sesión
    }
  });
}

// Controlador para mostrar el formulario de inicio de sesión
function loginform(req, res) {
  res.render('iniciar_sesion');
}

function procesarInicioSesion(req, res) {
  const { usuario, contraseña } = req.body;
  const contraseñaCifrada = seguridad.btoa(contraseña);

  // Verificar las credenciales en la base de datos
  connection.query('SELECT * FROM usuarios WHERE nombre_usuario = ? AND password = ?', [usuario, contraseñaCifrada], (error, results) => {
    if (error) {
      console.error('Error al consultar la base de datos:', error);
      return res.render('iniciar_sesion', { error: 'Error al procesar el inicio de sesión' });
    }

    if (results.length > 0 && results[0].password === contraseñaCifrada) {
      // El inicio de sesión es exitoso
      console.log("El inicio de sesión es exitoso");
      // Verificar qué usuario inició sesión
      req.session.usuarioId = results[0].id;

      // Generar el token JWT
      const token = jwt.sign({ usuario: results[0].id }, config.jwt.secretKey, { expiresIn: config.jwt.expiresIn });

      // Almacenar el token en la sesión
      req.session.token = token;
      console.log(token)
      // Redirigir a la página de inicio
      return res.redirect('/home');
    } else {
      // El inicio de sesión falló
      return res.render('iniciar_sesion', { error: 'Credenciales inválidas' });
    }
  });
}


// Controlador para mostrar el formulario de agregar botella
function mostrarFormularioBotella(req, res) {
  res.render('botella');
}

function agregarBotella(req, res, token) {
  try {
    // Verificar y decodificar el token
    const decodedToken = jwt.verify(token, config.jwt.secretKey);

    // Aquí puedes acceder a los datos del token decodificado, por ejemplo, el usuario ID
    const idUsuario = decodedToken.usuario;

    // Obtener los datos del formulario
    const { nombre_producto, tipo_material, cantidad, fecha_botella } = limpiarRequest(req.body);

    // Validar que los valores no sean nulos
    if (!nombre_producto || !tipo_material || !cantidad || !fecha_botella) {
      return res.status(400).render('reciclaje', { error: 'Por favor, completa todos los campos' });
    }

    // Insertar el nuevo reciclaje en la tabla 'reciclaje'
    const nuevaBotella = {
      nombre_producto,
      tipo_material,
      cantidad,
      fecha_botella,
    };

    connection.query('INSERT INTO botella SET ?', nuevaBotella, (error, result) => {
      if (error) {
        console.error('Error al agregar la botella:', error);
        return res.status(500).render('botella', { error: 'Error al agregar la botella' });
      }

      // Obtener el ID de la botella insertada
      const idBotella = result.insertId;

      // Agregar el historial de botella
      agregarHistorialBotella(idUsuario, idBotella, (err, historialId) => {
        if (err) {
          console.error('Error al agregar el historial de botella:', err);
          return res.status(500).render('botella', { error: 'Error al agregar el historial de botella' });
        }

        console.log('Botella e historial agregados exitosamente');
        res.redirect('/historial_botella');
      });
    });
  } catch (error) {
    // Error al verificar o decodificar el token
    console.error('Error al verificar o decodificar el token:', error);
    return res.status(500).render('botella', { error: 'Error al verificar o decodificar el token' });
  }
}


// Controlador para mostrar el historial de botella
function mostrarHistorialBotella(req, res) {
  const sql = `
    SELECT historial_botella.id, usuarios.nombre_usuario AS usuario, botella.nombre_producto AS botella, historial_botella.fecha_registro
    FROM historial_botella
    INNER JOIN usuarios ON historial_botella.id_usuario = usuarios.id
    INNER JOIN botella ON historial_botella.id_botella = botella.id`;

  connection.query(sql, (error, results) => {
    if (error) {
      console.error('Error al obtener el historial de botella:', error);
      return res.status(500).render('historial_botella', { error: 'Error al obtener el historial de botella' });
    }

    const historialBotella = results.map((row) => ({
      id: row.id,
      usuario: row.usuario,
      botella: row.botella,
      fecha_registro: row.fecha_registro
    }));

    res.render('historial_botella', { botellas: historialBotella });
  });
}

// Controlador para agregar datos al historial
function agregarHistorialBotella(idUsuario, idBotella, callback) {
  const sql = 'INSERT INTO historial_botella (id_usuario, id_botella) VALUES (?, ?)';
  const values = [idUsuario, idBotella];

  connection.query(sql, values, (error, result) => {
    if (error) {
      console.error('Error al agregar el historial de botella:', error);
      return callback(error);
    }

    console.log('Historial de botella agregado exitosamente');
    callback(null, result.insertId);
  });
}
// Controlador para mostrar el perfil del usuario
function mostrarPerfil(req, res) {
  const usuarioId = req.session.usuarioId;

  const sql = `
    SELECT * FROM usuarios
    WHERE id = ?`;

  connection.query(sql, [usuarioId], (error, results) => {
    if (error) {
      console.error('Error al obtener el perfil del usuario:', error);
      return res.status(500).render('perfil', { error: 'Error al obtener el perfil del usuario' });
    }

    if (results.length === 0) {
      return res.status(404).render('perfil', { error: 'Usuario no encontrado' });
    }

    const usuario = results[0];

    res.render('perfil', { usuario });
  });
}
// Controlador para mostrar el formulario de edición de perfil
function mostrarFormularioEdicionPerfil(req, res) {
  // Aquí puedes obtener los datos del usuario desde la base de datos y pasarlos a la vista
  const usuarioId = req.session.usuarioId;

  // Realiza la consulta para obtener los datos del usuario
  connection.query('SELECT * FROM usuarios WHERE id = ?', [usuarioId], (error, results) => {
    if (error) {
      console.error('Error al obtener los datos del usuario:', error);
      return res.status(500).render('editar_perfil', { error: 'Error al obtener los datos del usuario' });
    }

    if (results.length === 0) {
      return res.status(404).render('editar_perfil', { error: 'Usuario no encontrado' });
    }

    const usuario = results[0];

    // Renderiza la vista del formulario de edición de perfil con los datos del usuario
    res.render('editar_perfil', { usuario, error: null });
  });
}

// Controlador para procesar la edición de perfil
function procesarEdicionPerfil(req, res) {
  const usuarioId = req.session.usuarioId;
  const { nombre, apellido, correo } = req.body;

  // Realiza la actualización de los datos del usuario en la base de datos
  connection.query(
    'UPDATE usuarios SET nombre = ?, apellido = ?, correo = ? WHERE id = ?',
    [nombre, apellido, correo, usuarioId],
    (error) => {
      if (error) {
        console.error('Error al actualizar los datos del usuario:', error);
        return res.status(500).render('editar_perfil', { error: 'Error al actualizar los datos del usuario' });
      }

      // Redirige a la página del perfil del usuario después de la edición
      res.redirect('/perfil');
    }
  );
}

module.exports = {
  registroform,
  procesarRegistro,
  loginform,
  procesarInicioSesion,
  mostrarFormularioBotella,
  mostrarHistorialBotella,
  agregarBotella,
  agregarHistorialBotella,
  mostrarPerfil,
  mostrarFormularioEdicionPerfil,
  procesarEdicionPerfil
};
