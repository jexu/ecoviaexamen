const express = require('express');
const router = express.Router();
const { mostrarPaginaInicio } = require('../controllers/homeController');
const { verificarAutenticacion } = require('../middlewares/verificarAutenticacion');
const { registroform, procesarRegistro, loginform, procesarInicioSesion, agregarBotella, mostrarFormularioBotella, mostrarHistorialBotella,mostrarPerfil
,mostrarFormularioEdicionPerfil,procesarEdicionPerfil } = require('../controllers/mainController');
const csrf = require('csurf');

// Configurar csurf
const csrfProtection = csrf({ cookie: true });

// Ruta para mostrar el formulario de registro
router.get('/registro', csrfProtection, registroform);

// Ruta para procesar el registro
router.post('/registro', csrfProtection, procesarRegistro);

// Ruta para mostrar la página de inicio
router.get('/', verificarAutenticacion, csrfProtection, mostrarPaginaInicio);
router.get('/home', verificarAutenticacion, csrfProtection, mostrarPaginaInicio);

// Ruta para mostrar el formulario de inicio de sesión
router.get('/iniciar_sesion', csrfProtection, loginform);

// Ruta para procesar el inicio de sesión
router.post('/iniciar_sesion', csrfProtection, (req, res) => {
  procesarInicioSesion(req, res, (error, token) => {
    if (error) {
      console.error('Error en el inicio de sesión:', error);
      res.render('iniciar_sesion', { error: 'Error en el inicio de sesión' });
    } else {
      res.redirect('/home');
    }
  });
});

// Ruta para agregar una botella
router.post('/botella/agregar', csrfProtection, verificarAutenticacion, (req, res) => {
  agregarBotella(req, res, req.session.token);
});

// Ruta para cerrar sesión
router.get('/logout', csrfProtection, (req, res) => {
  req.session.destroy((error) => {
    if (error) {
      console.error('Error al cerrar sesión:', error);
    }
    res.redirect('/home'); // Redirige a la página de inicio después de cerrar sesión
  });
});

// Ruta para mostrar el formulario de agregar reciclaje
router.get('/botella/agregar', verificarAutenticacion, csrfProtection, mostrarFormularioBotella);

// Ruta de historial
router.get('/historial_botella', verificarAutenticacion, csrfProtection, mostrarHistorialBotella);
router.get('/perfil', verificarAutenticacion, mostrarPerfil);

// Ruta para mostrar el formulario de edición de perfil
router.get('/perfil/editar', verificarAutenticacion, csrfProtection, mostrarFormularioEdicionPerfil);

// Ruta para procesar la edición de perfil
router.post('/perfil/editar', verificarAutenticacion, csrfProtection, procesarEdicionPerfil);
module.exports = router;
